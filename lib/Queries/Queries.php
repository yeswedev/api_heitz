<?php

/*
 * Copyright (C) 2016 YesWeDev.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace ApiHeitz\Queries;

use ApiHeitz\ApiHeitzResponse;
use ApiHeitz\ApiHeitz,
    ApiHeitz\Exceptions\ApiHeitzException,
    ApiHeitz\Credential\Credential;

class Queries extends ApiHeitz
{

    private $credential;

    static public $articleCategories = [
        'ARTICLE' => 1,
        'ACCES' => 2,
        'REGROUPEMENT' => 4,
        'TICKET_CDB' => 8,
        'PACK' => 16,
        'AVOIR' => 32,
        'FRAIS' => 64,
        'ASSURANCE' => 128,
        'CADEAU' => 256
    ];


    /**
     * @access public
     * @param Credential $credential
     * @return $this
     */
    public function __construct(Credential $credential)
    {
        parent::__construct($credential);
        $this->credential = $credential;
        return $this;
    }

    /**
     * @access public
     * @param int idcentre(optional)
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20003
     */
    public function getTaskList($idCentre = NULL)
    {
        if ($idCentre !== null) {
            $param = ['idCentre' => $idCentre];
        } else {
            $param = null;
        }

        return $this->callWSHeitz((array)$this->query(20003, $param));
    }

    /**
     * @access public
     * @param int $idRequete
     * @param int $idCentre
     * @param int $nombreJourPlanning
     * @param \DateTime $datePlanning
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20004
     *
     */
    public function getReservationListAvailable($idRequete = 0, $idCentre, $nombreJourPlanning = 7, \DateTime $datePlanning)
    {
        $array = [
            'idRequete' => $idRequete,
            'idCentre' => $idCentre,
            'nombreJourPlanning' => $nombreJourPlanning,
            'datePlanning' => $datePlanning->format('d-m-y')
        ];

        return $this->callWSHeitz((array)$this->query(20004, $array));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20005
     */
    public function getActiveReservation()
    {
        return $this->callWSHeitz((array)$this->query(20005));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20006
     */
    public function getFormTraining()
    {
        return $this->callWSHeitz((array)$this->query(20006));
    }

    /**
     * @access public
     * @param int $idRequete
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20007
     */
    public function getDetailOfTheFormTraining($idRequete)
    {
        if (empty($idRequete)) {

            throw new ApiHeitzException(__METHOD__ . ' An element is empty', 9);
        } else {
            $array = [
                'idRequete' => $idRequete
            ];
            return $this->callWSHeitz((array)$this->query(20007, $array));
        }
    }

    /**
     * @access public
     * @param int $idRequete
     * @return ApiHeitzResponse query with id 20008
     * @throws ApiHeitzException
     */
    public function getDetailOfBilan($idRequete)
    {
        if (empty($idRequete)) {

            throw new ApiHeitzException(__METHOD__ . ' An element is empty', 9);
        } else {
            $array = [
                'idRequete' => $idRequete
            ];
            return $this->callWSHeitz((array)$this->query(20008, $array));
        }
    }

    /**
     * @access public
     * @param int $idRequete
     * @return ApiHeitzResponse query with id 20009
     * @throws ApiHeitzException
     */
    public function getDetailOfTest($idRequete)
    {
        if (empty($idRequete)) {

            throw new ApiHeitzException(__METHOD__ . ' --> An element is empty in the method !', 9);
        } else {
            $array = [
                'idRequete' => $idRequete
            ];
            return $this->callWSHeitz((array)$this->query(20009, $array));
        }
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20010
     */
    public function getListOfCures()
    {
        return $this->callWSHeitz((array)$this->query(20010));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20011
     */
    public function getDetailOfCures()
    {
        return $this->callWSHeitz((array)$this->query(20011));
    }

    /**
     * @access public
     * @param $idRequete
     * @param \DateTime $dateStart
     * @param int $numberOfDay
     * @param array $groups
     * @param array $task
     * @param array $dates
     * @param array $periodes
     * @param array $dayOfWeek
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20012
     */
    public function getListOfActiveReservationsForATaskAndADate($idRequete, \DateTime $dateStart, $numberOfDay = 1, array $groups, array $task, array $dates, array $periodes, array $dayOfWeek)
    {

        $array = [
            'idRequete' => $idRequete,
            'debut' => $dateStart->format('d-m-Y'),
            'nombreJour' => $numberOfDay,
            'groupes' => $groups,
            'taches' => $task,
            'dates' => $dates,
            'periodes' => $periodes,
            'joursSemaine' => $dayOfWeek,
        ];
        return $this->callWSHeitz((array)$this->query(20012, $array));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20013
     */
    public function getConfigServeur()
    {
        return $this->callWSHeitz((array)$this->query(20013));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20014
     */
    public function getBilan()
    {
        return $this->callWSHeitz((array)$this->query(20014));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20015
     */
    public function getTest()
    {
        return $this->callWSHeitz((array)$this->query(20015));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20016
     */
    public function getOneClient()
    {
        return $this->callWSHeitz((array)$this->query(20016));
    }

    /**
     * @access public
     * @param int $typeMessage 10
     * @param int $idClientMessage
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20017
     */
    public function getClientMessage($typeMessage = 10, $idClientMessage)
    {
        $fields = [
            'typeMessage' => $typeMessage,
            'idClientMessage' => $idClientMessage,
        ];
        return $this->callWSHeitz((array)$this->query(20017, $fields));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20018
     */
    public function getListOfActiveAccess()
    {
        return $this->callWSHeitz((array)$this->query(20018));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20019
     */
    public function getListOfPassages()
    {
        return $this->callWSHeitz((array)$this->query(20019));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20020
     */
    public function getListOfLevies()
    {
        return $this->callWSHeitz((array)$this->query(20020));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20021
     */
    public function getListOfFinancialDeadlines()
    {
        return $this->callWSHeitz((array)$this->query(20021));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20022
     */
    public function getListOfBills()
    {
        return $this->callWSHeitz((array)$this->query(20022));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20023
     */
    public function getListOfFinancial()
    {
        return $this->callWSHeitz((array)$this->query(20023));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20024
     */
    public function getListOfPoints()
    {
        return $this->callWSHeitz((array)$this->query(20024));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20025
     */
    public function getScheduleSEPA()
    {
        return $this->callWSHeitz((array)$this->query(20025));
    }


    /**
     * @param \DateTime $smallDateStart
     * @param \DateTime $smallDateEnd
     * @return ApiHeitzResponse
     * @throws ApiHeitzException
     */
    public function getScheduleUser(\DateTime $smallDateStart, \DateTime $smallDateEnd)
    {
        if (!($smallDateStart && $smallDateEnd)) {

            throw new ApiHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        }

        $array = [
            'debut' => $smallDateStart->format('d-m-Y'),
            'fin' => $smallDateEnd->format('d-m-Y'),
        ];
        return $this->callWSHeitz((array)$this->query(20030, $array));
    }

    /**
     * @access public
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @return ApiHeitzResponse query with id 20031
     * @throws ApiHeitzException
     */
    public function getDashboard(\DateTime $dateStart, \DateTime $dateEnd)
    {
        if (!($dateStart && $dateEnd)) {

            throw new ApiHeitzException(__METHOD__ . ' --> An element is empty params !', 9);
        }
        $fields = [
            'debut' => $dateStart->format('d-m-Y'),
            'fin' => $dateEnd->format('d-m-Y'),
        ];
        return $this->callWSHeitz((array)$this->query(20031, $fields));
    }

    /**
     * @access public
     * @param \DateTime $date
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20032
     */
    public function getDashbordFinancial(\DateTime $date)
    {
        $array = [];

        if (isset($date)) {
            $array['date'] = $date->format('d-m-Y');
        } else {
            throw new ApiHeitzException('Date is empty !', 9);
        }

        return $this->callWSHeitz((array)$this->query(20032, $array));
    }

    /**
     * @access public
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20033
     */
    public function getFinancial(\DateTime $dateStart, \DateTime $dateEnd)
    {
        if (!($dateStart && $dateEnd)) {
            throw new ApiHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $fields = [
                'debut' => $dateStart->format('d-m-Y'),
                'fin' => $dateEnd->format('d-m-Y'),
            ];
            return $this->callWSHeitz((array)$this->query(20033, $fields));
        }
    }

    /**
     * @access public
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20034
     */
    public function getSummaryEmployee(\DateTime $dateStart, \DateTime $dateEnd)
    {
        if (!($dateStart && $dateEnd)) {

            throw new ApiHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $array = [
                'debut' => $dateStart->format('d-m-Y'),
                'fin' => $dateEnd->format('d-m-Y')
            ];
            return $this->callWSHeitz((array)$this->query(20034, $array));
        }
    }

    /**
     * @access public
     * @param int $idRequete
     * @param int $pointIDClient
     * @return ApiHeitzResponse query with id 20035
     * @throws ApiHeitzException
     */
    public function getReadingPointsBalance($idRequete, $pointIDClient)
    {
        if (!($idRequete && $pointIDClient)) {

            throw new ApiHeitzException(__METHOD__ . ' --> An element is empty in the methode !', 9);
        } else {
            $fields = [
                'idRequete' => $idRequete,
                'pointIDClient' => $pointIDClient
            ];
            return $this->callWSHeitz((array)$this->query(20035, $fields));
        }
    }

    /**
     * @access public
     * @param int $idRequete
     * @param int $pointIDClient
     * @param int $point
     * @return ApiHeitzResponse query with id 20036
     * @throws ApiHeitzException
     */
    public function removingPointOfACustomer($idRequete, $pointIDClient, $point)
    {
        if (!($idRequete && $pointIDClient && $point)) {

            throw new ApiHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $array = [
                'idRequete' => $idRequete,
                'pointIDClient' => $pointIDClient,
                'point' => $point
            ];
            return $this->callWSHeitz((array)$this->query(20036, $array));
        }
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20101
     */
    public function getAllCivility()
    {
        return $this->callWSHeitz((array)$this->query(20101));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20102
     */
    public function getAllJobs()
    {
        return $this->callWSHeitz((array)$this->query(20102));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20103
     */
    public function getAllCity()
    {
        return $this->callWSHeitz((array)$this->query(20103));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20104
     */
    public function getAllFamilySituation()
    {
        return $this->callWSHeitz((array)$this->query(20104));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20105
     */
    public function getAllEmployee()
    {
        return $this->callWSHeitz((array)$this->query(20105));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20106
     */
    public function getAllCustomerGroup()
    {
        return $this->callWSHeitz((array)$this->query(20106));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20107
     */
    public function getAllDiscountLevel()
    {
        return $this->callWSHeitz((array)$this->query(20107));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20108
     */
    public function getAllTypeOfProspects()
    {
        return $this->callWSHeitz((array)$this->query(20108));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20109
     */
    public function getAllWayToKnowTheInstitution()
    {
        return $this->callWSHeitz((array)$this->query(20109));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20110
     */
    public function getAllWhereTheCustomerHasPracticed()
    {
        return $this->callWSHeitz((array)$this->query(20110));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20111
     */
    public function getAllMotivation()
    {
        return $this->callWSHeitz((array)$this->query(20111));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20112
     */
    public function getCriterion1()
    {
        return $this->callWSHeitz((array)$this->query(20112));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20113
     */
    public function getCriterion2()
    {
        return $this->callWSHeitz((array)$this->query(20113));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20114
     */
    public function getCriterion3()
    {
        return $this->callWSHeitz((array)$this->query(20114));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20115
     */
    public function getCriterion4()
    {
        return $this->callWSHeitz((array)$this->query(20115));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20116
     */
    public function getCriterion5()
    {
        return $this->callWSHeitz((array)$this->query(20116));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20117
     */
    public function getAllArticles()
    {
        return $this->callWSHeitz((array)$this->query(20117));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20118
     */
    public function getAllVAT()
    {
        return $this->callWSHeitz((array)$this->query(20118));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20119
     */
    public function getAllPayment()
    {
        return $this->callWSHeitz((array)$this->query(20119));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20120
     */
    public function getArrowPoints()
    {
        return $this->callWSHeitz((array)$this->query(20120));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20121
     */
    public function getTaskGroup()
    {
        return $this->callWSHeitz((array)$this->query(20121));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20122
     */
    public function getPlaceForTheTask()
    {
        return $this->callWSHeitz((array)$this->query(20122));
    }

    /**
     * @access public
     * @param string $email
     * @param string $oldPassword
     * @param string $newPassword
     * @param string $confirmPassword
     * @return ApiHeitzResponse the query with id 20201
     * @throws ApiHeitzException 'An element is empty in methode setPassword() !
     */
    public function setPassword($email, $oldPassword, $newPassword, $confirmPassword)
    {
        if (!($email && $oldPassword && $newPassword && $confirmPassword)) {

            throw new ApiHeitzException(__METHOD__ . ' --> An element is empty in the method !', 9);
        } else {
            $this->lenghtOfString($newPassword, 20);
            $fields = [
                'email' => $email,
                'code' => $oldPassword,
                'nouveau' => $newPassword,
                'confirmation' => $confirmPassword
            ];

            return $this->callWSHeitz((array)$this->query(20201, $fields));
        }
    }

    /**
     * @param int $id
     * @throws ApiHeitzException
     * @return ApiHeitzResponse the query with id 20202
     */
    public function getMessage($id)
    {

        if (!($id)) {

            throw new ApiHeitzException(__METHOD__ . ' --> Param is empty !', 9);
        } else {
            $field = [
                'idRequete' => $id,
            ];
            return $this->callWSHeitz((array)$this->query(20202, $field));
        }
    }

    /**
     * @access public
     * @param array $array
     * @return ApiHeitzResponse query with id 20203
     * @throws ApiHeitzException 'Bad array !'
     */
    public function setClient(array $array)
    {
        if (count($array) <= 21) {
            return $this->callWSHeitz((array)$this->query(20203, $array));
        } else {
            throw new ApiHeitzException(__METHOD__ . ' -->  Bad array !', 10);
        }
    }

    /**
     * @param int $id
     * @param bool $state
     * @param string $describ
     * @return ApiHeitzResponse query with id 20204
     * @throws ApiHeitzException
     */
    public function setReservation($id, $state, $describ)
    {
        if (!($id && $state && $describ)) {
            $fields = [
                'idRequete' => $id,
                'annulation' => $state,
                'raisonAnnulation' => $describ,
            ];
            return $this->callWSHeitz((array)$this->query(20204, $fields));
        } else {
            throw new ApiHeitzException(__METHOD__ . ' -->  Bad array !', 10);
        }
    }

    /**
     * @access public
     * @param array $array
     * @return ApiHeitzResponse query with id 20301
     * @throws ApiHeitzException 'Bad array !'
     */
    public function addReservation(array $array)
    {
        if (count($array) == 6) {
            return $this->callWSHeitz((array)$this->query(20301, $array));
        } else {
            throw new ApiHeitzException(__METHOD__ . ' --> Bad array !', 10);
        }
    }

    /**
     * @access public
     * @param array $array
     * @return ApiHeitzResponse query with id 20302
     * @throws ApiHeitzException 'Bad array !'
     */
    public function addSale(array $array)
    {
        if (count($array) == 2) {
            return $this->callWSHeitz((array)$this->query(20302, $array));
        } else {
            throw new ApiHeitzException(__METHOD__ . ' --> Bad array', 10);
        }
    }

    /**
     * @access public
     * @param array $array
     * @return ApiHeitzResponse query with id 20303
     * @throws ApiHeitzException 'Bad array !'
     */
    public function addClient(array $array)
    {
        if (count($array) >= 4) {
            return $this->callWSHeitz((array)$this->query(20303, $array));
        } else {
            throw new ApiHeitzException(__METHOD__ . ' -->  Bad array !', 10);
        }
    }

    /**
     * @access public
     * @param string email
     * @return ApiHeitzResponse query with id 20401
     * @throws ApiHeitzException 'Email is empty !'
     */
    public function addPassword($email)
    {
        $field = [];
        if (!empty($email) && (filter_var($email, FILTER_VALIDATE_EMAIL))) {
            $field['email'] = $email;
        } else {
            throw new ApiHeitzException(__METHOD__ . ' --> Email is empty or invalid !', 4);
        }

        return $this->callWSHeitz((array)$this->query(20401, $field));
    }

    /**
     * @access public
     * @param array $array
     * @param string $operateurChaine
     * @param int $nombreEnregistrement
     * @return ApiHeitzResponse query with id 20501
     * @throws ApiHeitzException 'Item "operateurChaine" in empty in searchClient() !'
     */
    public function searchClient(array $array, $operateurChaine, $nombreEnregistrement = null)
    {
        if ((!array_key_exists('operateurChaine', $array)) || ($operateurChaine !== null)) {

            if (($nombreEnregistrement !== null) && is_integer($nombreEnregistrement)) {
                $array['nombreEnregistrement'] = $nombreEnregistrement;
            }

            $array['operateurChaine'] = $operateurChaine;
        } else {
            throw new ApiHeitzException(__METHOD__ . ' --> Item "operateurChaine" is empty !', 22);
        }
        return $this->callWSHeitz((array)$this->query(20501, $array));
    }

    /**
     * @access public
     * @param string $sql
     * @return ApiHeitzResponse query with id 20502
     * @throws ApiHeitzException 'Query SQL is empty !'
     */
    public function SQLQuery($sql)
    {
        $array = [];
        if (!empty($sql)) {

            $array['requeteSQL'] = $sql;
        } else {
            throw new ApiHeitzException(__METHOD__ . ' --> Query SQL is empty !', 30);
        }

        return $this->callWSHeitz((array)$this->query(20502, $array));
    }

    /**
     * @access public
     * @param array $task
     * @param array $dates
     * @param array $periods
     * @throws ApiHeitzException
     * @return ApiHeitzResponse
     */
    public function listOfActiveSites(array $task, array $dates, array $periods)
    {
        $fields = [
            'taches' => $task,
            'dates' => $dates,
            'periodes' => $periods,
        ];
        return $this->callWSHeitz((array)$this->query(21001, $fields));
    }

    /**
     * @access public
     * @param string $key
     * @param string $value
     * @return ApiHeitzResponse query with id 21002
     * @throws ApiHeitzException an element is empty in params
     */
    public function setDataClients($key, $value)
    {
        if (!($key && $value)) {
            throw new ApiHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $fields = [
                'cle' => $key,
                'valeur' => $value,
            ];
            return $this->callWSHeitz((array)$this->query(21002, $fields));
        }
    }

    /**
     * @access public
     * @param string $key
     * @return ApiHeitzResponse query with id 21003
     * @throws ApiHeitzException An element is empty in param !
     */
    public function getDataClients($key)
    {
        if (!($key)) {
            throw new ApiHeitzException(__METHOD__ . ' --> An element is empty in param !', 9);
        } else {
            $field = [
                'cle' => $key,
            ];
            return $this->callWSHeitz((array)$this->query(21003, $field));
        }
    }

    /**
     * @access public
     * @param int $idClient
     * @param string $newPassword
     * @return ApiHeitzResponse query with id 21005
     * @throws ApiHeitzException An element is empty in params !
     */
    public function setNewPasswordForClientId($idClient, $newPassword)
    {
        if (!($idClient && $newPassword)) {
            throw new ApiHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $fields = [
                'idClient' => $idClient,
                'nouveau' => $newPassword,
            ];
            return $this->callWSHeitz((array)$this->query(21005, $fields));
        }
    }

    /**
     * @access public
     * @abstract the current user must be an employee
     * @param int $idQuery
     * @param int $idClient
     * @param int $point
     * @param string $describ
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 21004
     */
    public function addPointsForClient($idQuery, $idClient, $point, $describ)
    {
        if (!($idQuery && $idClient && $point && $describ)) {

            throw new ApiHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $fields = [
                'idRequete' => $idQuery,
                'pointIDClient' => $idClient,
                'point' => $point,
                'libelle' => $describ,
            ];
            return $this->callWSHeitz((array)$this->query(21004, $fields));
        }
    }

    /**
     * @access public
     * @param array $sales
     * @param string $return URL of return
     * @param string $cssClassButton
     * @return ApiHeitzResponse query with id 21006
     * @throws ApiHeitzException
     */
    public function payboxForm(array $sales, $return, $cssClassButton = null)
    {
        if (!($sales && $return)) {

            throw new ApiHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $fields = [
                'ventes' => $sales,
                'retour' => $return,
                'classeBouton' => $cssClassButton,
            ];
            return $this->callWSHeitz((array)$this->query(21006, $fields));
        }
    }

    /**
     * @access public
     * @param string $refTransaction
     * @param string $sellerId
     * @param string $deal
     * @param string $errorCode
     * @param string $HMACkey
     * @return ApiHeitzResponse query with id 21007
     * @throws ApiHeitzException An element is empty in params !
     */
    public function payboxValidation($refTransaction, $sellerId, $deal, $errorCode, $HMACkey)
    {
        if (!($refTransaction && $sellerId && $deal && $errorCode && $HMACkey)) {

            throw new ApiHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $fields = [
                'reference' => $refTransaction,
                'identification' => $sellerId,
                'transaction' => $deal,
                'erreur' => $errorCode,
                'hmac' => $HMACkey,
            ];
            return $this->callWSHeitz((array)$this->query(21007, $fields));
        }
    }

    /**
     * @access public
     * @param string $bankName
     * @param string $iban
     * @param string $bic
     * @param int $id
     * @return ApiHeitzResponse query with id 21008
     * @throws ApiHeitzException
     */
    public function addBankAccount($bankName, $iban, $bic, $id)
    {
        if (!($bankName && $iban && $bic && $id)) {

            throw new ApiHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $fields = [
                'nomBanque' => $bankName,
                'iban' => $iban,
                'bic' => $bic,
                'idBanque' => $id,
            ];
            return $this->callWSHeitz((array)$this->query(21008, $fields));
        }
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 21009
     */
    public function validateSession()
    {
        return $this->callWSHeitz((array)$this->query(21009));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 21010
     */
    public function getConfigApp()
    {
        return $this->callWSHeitz((array)$this->query(21010));
    }

    /**
     * @param int $idTask
     * @param int $idLocation
     * @param int $idEmploye
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @param string $note
     * @param int $slots
     * @param int $idSerie
     * @param int $idEtablissement
     * @return ApiHeitzResponse query with id 21011
     * @throws ApiHeitzException
     */
    public function addSchedule($idTask, $idLocation, $idEmploye = null, \DateTime $dateStart, \DateTime $dateEnd, $note = null, $slots, $idSerie = null, $idEtablissement = null)
    {
        if (!($idTask && $idLocation && $dateStart && $dateEnd && $slots)) {
            throw new ApiHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $fields = [
                'idTache' => (int)$idTask,
                'idLieu' => (int)$idLocation,
                'idEmploye' => $idEmploye,
                'debut' => $dateStart->format('d-m-Y H:i:s'),
                'fin' => $dateEnd->format('d-m-Y H:i:s'),
                'note' => $note,
                'place' => (int)$slots,
                'idSerie' => (int)$idSerie,
                'idEtablissement' => (int)$idEtablissement,
            ];

            return $this->callWSHeitz((array)$this->query(21011, $fields));
        }
    }

    /**
     * @param int $idSlot
     * @param int $idTask
     * @param int $idLocation
     * @param int $idEmploye
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @param string $note
     * @param int $slots
     * @param int $idSerie
     * @param int $idEtablissement
     * @return ApiHeitzResponse query with id 21012
     * @throws ApiHeitzException
     */
    public function setSchedule($idSlot, $idTask, $idLocation, $idEmploye = null, \DateTime $dateStart, \DateTime $dateEnd, $note = null, $slots, $idSerie = null, $idEtablissement = null)
    {
        if (!($idSlot && $idTask && $idLocation && $dateStart && $dateEnd && $slots)) {
            throw new ApiHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $fields = [
                'idCreneau' => (int)$idSlot,
                'idTache' => (int)$idTask,
                'idLieu' => (int)$idLocation,
                'idEmploye' => $idEmploye,
                'debut' => $dateStart->format('d-m-Y H:i:s'),
                'fin' => $dateEnd->format('d-m-Y H:i:s'),
                'note' => $note,
                'place' => (int)$slots,
                'idSerie' => (int)$idSerie,
                'idEtablissement' => (int)$idEtablissement,
            ];

            return $this->callWSHeitz((array)$this->query(21012, $fields));
        }
    }

    /**
     * @param int $idSlot
     * @return ApiHeitzResponse query with id 21013
     * @throws ApiHeitzException
     */
    public function removeSchedule($idSlot)
    {
        if (!($idSlot)) {
            throw new ApiHeitzException(__METHOD__ . ' --> Id slot is empty !', 9);
        } else {
            $field = [
                'idCreneau' => (int)$idSlot,
            ];

            return $this->callWSHeitz((array)$this->query(21013, $field));
        }
    }

    /**
     * @param int $idSlot
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @return ApiHeitzResponse query with id 21014
     * @throws ApiHeitzException
     */
    public function listSchedule($idSlot, \DateTime $dateStart, \DateTime $dateEnd)
    {
        if (!($idSlot && $dateStart && $dateEnd)) {
            throw new ApiHeitzException(__METHOD__ . ' --> Id slot is empty !', 9);
        } else {
            $fields = [
                'idCreneau' => (int)$idSlot,
                'debut' => $dateStart->format('d-m-Y H:i:s'),
                'fin' => $dateEnd->format('d-m-Y H:i:s'),
            ];

            return $this->callWSHeitz((array)$this->query(21014, $fields));
        }
    }

    /**
     * @param int $idSlot
     * @param int $idEmploye
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @return ApiHeitzResponse query with id 21015
     * @throws ApiHeitzException
     */
    public function cancelSchedule($idSlot, $idEmploye, \DateTime $dateStart, \DateTime $dateEnd)
    {
        if (!($idSlot && $idEmploye && $dateStart && $dateEnd)) {
            throw new ApiHeitzException(__METHOD__ . ' --> Id slot is empty !', 9);
        } else {
            $fields = [
                'idCreneau' => (int)$idSlot,
                'idEmploye' => (int)$idEmploye,
                'debut' => $dateStart->format('d-m-Y H:i:s'),
                'fin' => $dateEnd->format('d-m-Y H:i:s'),
            ];

            return $this->callWSHeitz((array)$this->query(21015, $fields));
        }
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 21016
     */
    public function getAllBank()
    {
        return $this->callWSHeitz((array)$this->query(21016));
    }

    /**
     * @access public
     * @param string $society
     * @param string $code
     * @param string $bic
     * @param string $countryCode
     * @return ApiHeitzResponse query with id 21017
     * @throws ApiHeitzException
     */
    public function addBank($society, $code, $bic, $countryCode)
    {
        if (!($society && $code && $bic && $countryCode)) {
            throw new ApiHeitzException(__METHOD__ . ' --> on param is empty !', 9);
        } else {
            $fields = [
                'societe' => $society,
                'code' => $code,
                'bic' => $bic,
                'CodePays' => $countryCode,
            ];

            return $this->callWSHeitz((array)$this->query(21017, $fields));
        }

    }

    /**
     *
     * @access public
     * @param string $city
     * @param string $zipCode
     * @param string $country
     * @return ApiHeitzResponse query with id 21018
     * @throws ApiHeitzException
     */
    public function addCity($city, $zipCode, $country)
    {
        if (!($city && $zipCode && $country)) {
            throw new ApiHeitzException(__METHOD__ . ' --> on param is empty !', 9);
        } else {
            $fields = [
                'ville' => $city,
                'codePostal' => $zipCode,
                'pays' => $country,
            ];

            return $this->callWSHeitz((array)$this->query(21018, $fields));
        }
    }

    /**
     * @access public
     * @param string $email
     * @return ApiHeitzResponse query with id 21019
     * @throws ApiHeitzException
     */
    public function existingEmail($email)
    {
        if (!empty($email) && (filter_var($email, FILTER_VALIDATE_EMAIL))) {
            throw new ApiHeitzException(__METHOD__ . ' --> on param is invalid !', 9);
        } else {
            $fields = [
                'email' => $email,
            ];

            return $this->callWSHeitz((array)$this->query(21019, $fields));
        }
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse
     */
    public function clientPresent()
    {
        return $this->callWSHeitz((array)$this->query(21020));
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * * @return ApiHeitzResponse
     */
    public function getAllTokens()
    {
        return $this->callWSHeitz((array)$this->query(21021));
    }

    /**
     * @access public
     * @param int $id
     * @param string $phoneCode
     * @return ApiHeitzResponse query with id 21022
     * @throws ApiHeitzException
     */
    public function tokenActivation($id, $phoneCode)
    {
        if (!($id && $phoneCode)) {
            throw new ApiHeitzException(__METHOD__ . ' --> on param is invalid !', 9);
        } else {
            $fields = [
                'id' => $id,
                'phoneCode' => $phoneCode,
            ];

            return $this->callWSHeitz((array)$this->query(21022, $fields));
        }
    }

    /**
     * @access public
     * @param int $id
     * @param string $phoneCode
     * @return ApiHeitzResponse query with id 21023
     * @throws ApiHeitzException
     */
    public function tokenRevocation($id, $phoneCode)
    {
        if (!($id && $phoneCode)) {
            throw new ApiHeitzException(__METHOD__ . ' --> on param is invalid !', 9);
        } else {
            $fields = [
                'id' => $id,
                'phoneCode' => $phoneCode,
            ];

            return $this->callWSHeitz((array)$this->query(21023, $fields));
        }
    }

    /**
     * @access public
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 21025
     */
    public function getCodePassing()
    {
        return $this->callWSHeitz((array)$this->query(21025));
    }

    /**
     *
     * @param int $idType
     * @param array $fields
     * @throws ApiHeitzException
     * @return ApiHeitzResponse
     */
    public function genericQuery($idType, array $fields)
    {
        return $this->callWSHeitz((array)$this->query($idType, $fields));
    }

    /**
     * @access public
     * @param string Path to Cookie
     * @throws ApiHeitzException
     * @return ApiHeitzResponse query with id 20402
     */
    public function disconnect($path = 'tmp/cookie.txt')
    {
        $fp = fopen(realpath($path), 'r+');
        ftruncate($fp, 0);
        fclose($fp);
        return $this->callWSHeitz((array)$this->query(20402));
    }


    /**
     * @return ApiHeitzResponse
     * @throws \ApiHeitz\Exceptions\ApiHeitzException
     */
    public function getCenterInfo()
    {
        return $this->callHeitzAPI('center/info', 'GET', ['fields' => 'id,name,address1,address2,cp,city,phone,fax,email,owner,generator,siren,siret,ape,rcs,companyLegalForm,taxNumber,logo,capacity,site,geoLatitude,geoLongitude,country,capitalSocial']);
    }

    /**
     * @return ApiHeitzResponse
     * @throws \ApiHeitz\Exceptions\ApiHeitzException
     */
    public function getTokenDeveloper($data)
    {
        return $this->callHeitzAPI('developer/getToken', 'POST', $data);
    }

    /**
     * @return ApiHeitzResponse
     * @throws \ApiHeitz\Exceptions\ApiHeitzException
     */
    public function getArticles()
    {
        return $this->callHeitzAPI('articles?_limit=60', 'GET', []);
    }

    /**
     * @return ApiHeitzResponse
     * @throws \ApiHeitz\Exceptions\ApiHeitzException
     */
    public function getArticlePrices($idArticle)
    {
        return $this->callHeitzAPI('articles/' . $idArticle . '/tariffs', 'GET', []);
    }

    /**
     * @return ApiHeitzResponse
     * @throws \ApiHeitz\Exceptions\ApiHeitzException
     */
    public function getPaymentMethods()
    {
        return $this->callHeitzAPI('paymentMethod/list', 'GET', []);
    }

    /**
     * Creates a sale for a particular client.
     * @return ApiHeitzResponse
     * @throws \ApiHeitz\Exceptions\ApiHeitzException
     */
    public function createSales($data)
    {
        return $this->callHeitzAPI('sales', 'POST', $data);
    }

}
