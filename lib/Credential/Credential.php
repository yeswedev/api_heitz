<?php

/*
 * Copyright (C) 2015 YesWeDev.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace ApiHeitz\Credential;

use ApiHeitz\Exceptions\ApiHeitzException;

/**
 * Class Credential
 * @package ApiHeitz\Credential
 */
class Credential
{

    /**
     * @abstract port à définir suivant les besoins de la configuration du serveur WEB HEITZ, par défault c'est le port web standard 80
     * @var int
     */
    private $port = 80;
    /**
     * @var int
     */
    private $portV2 = 8080;

    /**
     *
     * @var string $host
     */
    private $host;

    /**
     *
     * @var string Host Password
     */
    private $hostPassword;

    /**
     *
     * @var string User Login
     */
    private $userLogin;

    /**
     *
     * @var string User pass
     */
    private $userPass;

    /**
     * @var
     */
    private $apiId;

    /**
     * @var
     */
    private $apiKey;

    /**
     *
     * @var string $urlWebServeurHeitz
     */
    private $urlWebServeurHeitz;

    /**
     *
     * @var string $urlHeitzAPI
     */
    private $urlHeitzAPI;

    /**
     * @access public
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @access public
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @access public
     * @return string
     */
    public function getHostPassword()
    {
        return $this->hostPassword;
    }

    /**
     * @access public
     * @return string
     */
    public function getUserLogin()
    {
        return $this->userLogin;
    }

    /**
     * @access public
     * @return string
     */
    public function getUserPass()
    {
        return $this->userPass;
    }

    /**
     * @access public
     * @return string
     */
    public function getApiId()
    {
        return $this->apiId;
    }

    /**
     * @param string $apiId
     * @return Credential
     */
    public function setApiId(string $apiId): Credential
    {
        $this->apiId = $apiId;
        return $this;
    }

    /**
     * @access public
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     * @return Credential
     */
    public function setApiKey(string $apiKey): Credential
    {
        $this->apiKey = $apiKey;
        return $this;
    }



    /**
     * @access public
     * @param int $port
     * @return Credential
     * @throws ApiHeitzException
     */
    public function setPort($port)
    {
        if (((!empty($port)) && (filter_var($port, FILTER_VALIDATE_INT) ) && ($port > 0 && $port <= 65535))) {
            $this->port = $port;
            return $this;
        } else {
            throw new ApiHeitzException(__METHOD__ . ' --> The port is not a interger or is empty !', 1);
        }
    }

    /**
     * @access public
     * @param string $host
     * @return Credential
     * @throws ApiHeitzException
     */
    public function setHost($host)
    {
        if (!empty($host)) {
            $this->host = $host;
            return $this;
        } else {
            throw new ApiHeitzException(__METHOD__ . ' --> Domain or IP are empty !', 2);
        }
    }

    /**
     * @access public
     * @param string $hostPassword
     * @return Credential
     * @throws ApiHeitzException
     */
    public function setHostPassword($hostPassword)
    {
        if (!empty($hostPassword)) {
            $this->hostPassword = $hostPassword;
            return $this;
        } else {
            throw new ApiHeitzException(__METHOD__ . ' --> Password is empty !', 3);
        }
    }

    /**
     * @access public
     * @param string $userLogin
     * @return string $userLogin
     * @throws ApiHeitzException
     */
    public function setUserLogin($userLogin)
    {
        if (!empty($userLogin) && (filter_var($userLogin, FILTER_VALIDATE_EMAIL))) {
            $this->userLogin = $userLogin;
            return $this;
        } else {
            throw new ApiHeitzException(__METHOD__ . ' --> Email is empty or invalid !', 4);
        }
    }

    /**
     * @access public
     * @param string $userPass
     * @return Credential
     * @throws ApiHeitzException
     */
    public function setUserPass($userPass)
    {
        if (!empty($userPass)) {
            $this->userPass = $userPass;
            return $this;
        } else {
            throw new ApiHeitzException(__METHOD__ . ' --> User pass is empty !', 5);
        }
    }

    /**
     * @return int
     */
    public function getPortV2()
    {
        return $this->portV2;
    }

    /**
     * @access public
     * @param int $portV2
     * @return Credential
     * @throws ApiHeitzException
     */
    public function setPortV2($portV2)
    {
        if (((!empty($portV2)) && (filter_var($portV2, FILTER_VALIDATE_INT) ) && ($portV2 > 0 && $portV2 <= 65535))) {
            $this->portV2 = $portV2;
            return $this;
        } else {
            throw new ApiHeitzException(__METHOD__ . ' --> The portV2 is not a interger or is empty !', 1);
        }
    }

    /**
     * @access public
     * @return string $urlWebServeurHeitz
     */
    public function urlWebServeurHeitz()
    {
        $this->urlWebServeurHeitz = (string) $this->getHost() . ':' . $this->getPort();
        return $this;
    }

    /**
     * @access public
     * @return string $urlWebServeurHeitz
     */
    public function urlHeitzAPI()
    {
        $this->urlHeitzAPI = (string) $this->getHost() . ':' . $this->getPortV2() .'/api/';
        return $this->urlHeitzAPI;
    }

    /**
     * @access public
     * @return string
     */
    public function __toString()
    {
        return $this->urlWebServeurHeitz;
    }

}
