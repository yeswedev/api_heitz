<?php

/*
 * Copyright (C) 2016 YesWeDev.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 * MA 02110-1301  USA
 *
 * @author YesWeDev <contacrarobaseyeswedev.fr>
 * @copyright (c) 2016
 * @license GNU/LGPL http://www.gnu.org/licenses/lgpl.html
 */

namespace ApiHeitz;

use ApiHeitz\ErrorDictionary\ErrorDictionary,
    ApiHeitz\Exceptions\ApiHeitzException,
    ApiHeitz\Credential\Credential,
    ApiHeitz\Core\CoreInterface;

class ApiHeitz implements CoreInterface
{

    /**
     *
     * @var string
     */
    private $connectHeitzAPI;

    /**
     *
     * @var int
     */
    private $idSession;

    /**
     *
     * @var int
     */
    private $idClient;

    /**
     *
     * @var bool
     */
    private $debug = false;

    /**
     *
     * @var string
     */
    private $credential;

    /**
     *
     * @var string
     */
    private $token;

    /**
     *
     * @var int
     */
    private $numberRow;

    /**
     *
     * @var string
     */
    private $phoneCode;

    /**
     *
     * @param Credential $credential
     * @return \ApiHeitz\ApiHeitz
     */
    public function __construct(Credential $credential)
    {
        $this->credential = $credential;
        return $this;
    }

    /**
     * @return object
     */
    public function getCredential()
    {
        return $this->credential;
    }

    /**
     * @access public
     * @return int
     */
    public function getIdSession()
    {
        return $this->idSession;
    }

    /**
     * @access public
     * @return int
     */
    public function getIdClient()
    {
        return $this->idClient;
    }

    /**
     * @access public
     * @return bool
     */
    public function getDebug()
    {
        return $this->debug;
    }

    /**
     * @return string
     */
    public function getConnectHeitzAPI()
    {
        return $this->connectHeitzAPI;
    }

    /**
     * @return int
     */
    public function getNumberRow()
    {
        return $this->numberRow;
    }

    /**
     *
     * @return string
     */
    public function getPhoneCode()
    {
        return $this->phoneCode;
    }

    /**
     * @param string $idSession
     * @return \ApiHeitz\ApiHeitz
     */
    public function setIdSession($idSession)
    {
        $this->idSession = $idSession;
        return $this;
    }

    /**
     * @access public
     * @param int $idClient idClient
     * @return $this
     * @throws ApiHeitzException
     */
    public function setIdClient($idClient)
    {
        if ((!empty($idClient)) && (filter_var($idClient, FILTER_VALIDATE_INT))) {
            $this->idClient = $idClient;
            return $this;
        } else {
            throw new ApiHeitzException(__METHOD__ . ' --> The Client ID is empty or is not an integer !', 6);
        }
    }

    /**
     * @access public
     * @param string $token token
     * @return $this
     * @throws ApiHeitzException
     */
    public function setToken($token)
    {
        if (!empty($token)) {
            $this->token = $token;
            return $this;
        } else {
            throw new ApiHeitzException(__METHOD__ . ' --> The TOKEN is empty or is not an integer !', 6);
        }
    }

    /**
     * @param bool $debug
     * @return \ApiHeitz\ApiHeitz
     * @throws ApiHeitzException
     */
    public function setDebug($debug)
    {

        if (filter_var($debug, FILTER_VALIDATE_BOOLEAN)) {

            $this->debug = $debug;
            return $this;
        } else {
            throw new ApiHeitzException(__METHOD__ . ' --> debug is not an boolean !', 10);
        }
    }

    /**
     * @access public
     * @param int $numberRow
     * @return \ApiHeitz\ApiHeitz
     * @throws ApiHeitzException
     */
    public function setNumberRow($numberRow)
    {
        if (((!empty($numberRow)) && (filter_var($numberRow, FILTER_VALIDATE_INT)))) {
            $this->numberRow = $numberRow;
            return $this;
        } else {
            throw new ApiHeitzException(__METHOD__ . ' --> The $numberRow is not a interger or is empty !', 1);
        }
    }

    /**
     * @access protected
     * @param bool $bool
     * @return string or Null
     */
    protected function getPhoto($bool)
    {
        if ($bool) {
            $photo = 'Client_Photo,';
        } else {
            $photo = null;
        }

        return $photo;
    }

    /**
     * @access private
     * @param string $JSON
     * @return ApiHeitzResponse
     * @throws ApiHeitzException
     */
    private function analyseReply($JSON)
    {
        if (!is_object($JSON)) {
            $JSON = new ApiHeitzResponse();
            $JSON->status = 'ko';
            $JSON->idErreur = '10';
        }

        if (!in_array('status', get_object_vars($JSON))) {

            $JSON = new ApiHeitzResponse();
            $JSON->status = 'ko';
            $JSON->idErreur = '10';
        }

        if ($this->getDebug()) {
            echo '<pre>';
            var_dump($JSON);
            echo '</pre>';
        }

        if ($JSON->status === 'ok') {
            return $JSON;
        } elseif ($JSON->status === 'ko') {

            $error = (new ErrorDictionary($JSON->idErreur))->getMessage();
            throw new ApiHeitzException(__METHOD__ . ' --> ' . $error, $JSON->idErreur);
        } elseif ($JSON === null) {

            throw new ApiHeitzException(__METHOD__ . ' --> webservice returns an incorrect format !', 7);
        }
    }

    /**
     * @access public
     * @param int $idSession
     * @param int $userId
     * @param int $type
     * @return $this array fields
     */
    public function setConnectHeitzAPI($idSession = 0, $userId = 0, $type = 1)
    {
        $fields = ['status' => 0,
            'idErreur' => 0,
            'idSite' => $this->credential->getHostPassword(),
            'type' => $type,
            'email' => $this->credential->getUserLogin(),
            'code' => $this->credential->getUserPass(),
            'idSession' => $idSession,
            'idClient' => $userId,
            'phoneCode' => $this->getPhoneCode(),
        ];
        $this->connectHeitzAPI = (array)$fields;
        return $this;
    }

    /**
     *
     * @param string $phoneCode
     * @return $this
     * @throws ApiHeitzException
     */
    public function setPhoneCode($phoneCode)
    {

        $this->phoneCode = $phoneCode;
        return $this;
    }

    /**
     * @access public
     * @param array fields
     * @param string pathToCookie
     * @return ApiHeitzResponse
     * @throws ApiHeitzException
     */
    public function callWSHeitz($newFields = null, $pathToCookie = 'tmp/cookie.txt')
    {
        try {
            $curl = curl_init($this->credential->urlWebServeurHeitz() . '/json');

            if ($newFields === null) {
                $newFields = $this->getConnectHeitzAPI();
            }

            if ($this->getDebug()) {
                var_dump($newFields, json_encode($newFields));
            }

            //initialistion des options de cURL
            $options = [
                CURLOPT_POSTFIELDS => json_encode($newFields),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_HTTPHEADER => ['Content-type: application/json'],
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_COOKIEJAR => realpath($pathToCookie),
                CURLOPT_COOKIEFILE => realpath($pathToCookie)
            ];

            // Bind des options et de l'objet cURL que l'on va utiliser
            curl_setopt_array($curl, $options);

            $return = curl_exec($curl); // réponse de la requète dans la variable $return
            //appel du gestionnaire des erreur de cURL
        } catch (\Exception $exc) {

            throw new ApiHeitzException(__METHOD__ . ' --> ' . $exc->getMessage(), $exc->getCode());
        }

        if (curl_errno($curl) > 0) {

            if ($this->getDebug()) {
                var_dump($curl);
            }

            $message = curl_error($curl);
            curl_close($curl);
            throw new ApiHeitzException(__METHOD__ . ' --> Value of cURL error: ' . $message, 8);
        } else {

            if ($this->getDebug()) {
                var_dump($curl);
            }

            curl_close($curl);
            return $this->analyseReply(json_decode($return));
        }
    }

    /**
     * @param $response
     * @return mixed
     * @throws ApiHeitzException
     */
    private function handleResponse($response)
    {
        $statusCode = intval($response->getStatusCode());
        if ($statusCode < 200 || $statusCode >= 300) {
            throw new ApiHeitzException('Problem getting calendar data !', $statusCode);
        }

        return json_decode($response->getBody()->getContents());
    }

    private function getHeaders()
    {
        $headers = ['Accept' => 'application/json'];
        if ($this->token && $this->token != '') {
            $headers ['Authorization'] = $this->token;
        }
        return $headers;
    }

    /**
     * @access public
     * @param array fields
     * @param string pathToCookie
     * @throws ApiHeitzException
     * @return ApiHeitzResponse
     */
    public function callHeitzAPI($url, $type, $body)
    {
        $client = new \GuzzleHttp\Client();
        if ($this->getDebug()) {
            echo 'calling API<br/>';
            echo $url . '<br/>';
            echo $type . '<br/>';
        }

        $fullUrl = $this->credential->urlHeitzAPI().$url;
        $params = ['headers' => $this->getHeaders()];
        if ($type == 'GET') {
            $first = true;
            foreach ($body as $key => $value) {
                $fullUrl .= ($first ? '?' : '&') . $key . '=' . $value;
                $first = false;
            }
        }
        else{
            $params['json'] = $body;
        }

        if ($this->getDebug()) {
            echo '<pre>';
            var_dump([$type,
                $fullUrl,
                $params]);
            echo '</pre>';
        }

        $response = $client->request(
            $type,
            $fullUrl,
            $params
        );

        if ($this->getDebug()) {
            echo '<pre>';
            var_dump([
                'statusCode' => $response->getStatusCode(),
                'headerLine' => $response->getHeaderLine('content-type'),
            ]);
            echo '</pre>';
        }

        return $this->handleResponse($response);
    }

    /**
     * @access protected
     * @param int $type
     * @param int $param
     * @return array
     * @throws ApiHeitzException
     */
    protected function query($type, $param = null)
    {
        if (!empty($type)) {

            $fields = [
                'status' => 'null',
                'idErreur' => 0,
                'idSite' => $this->credential->getHostPassword(),
                'idSession' => $this->getIdSession(),
                'idClient' => $this->getIdClient(),
                'type' => $type,
                'phoneCode' => $this->getPhoneCode(),
            ];

            if ($this->getNumberRow() > 0) {
                $fields['nombreLigne'] = $this->getNumberRow();
            }

            if (($param !== null) && (is_array($param))) {

                foreach ($param as $key => $value) {
                    $fields[$key] = $value;
                }
            }

            return (array)$fields;
        } else {
            throw new ApiHeitzException(__METHOD__ . ' --> The type is empty !', 9);
        }
    }

    /**
     * @access protected
     * @param string $string
     * @param int $maxSize
     * @return string
     * @throws ApiHeitzException
     */
    protected function lenghtOfString($string, $maxSize)
    {
        if (strlen($string) > $maxSize) {
            throw new ApiHeitzException(__METHOD__ . ' --> The string is greater than "' . $maxSize . '" characters !', 30);
        } else {
            return (string)$string;
        }
    }

}
